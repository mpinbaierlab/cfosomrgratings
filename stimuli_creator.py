import numpy as np
import math


def initiate_stimulus_saving():
    global stimulus_matrices
    stimulus_matrices = np.empty((0,7))

def add_stimulus_matrix(stimulus_matrix):
    global stimulus_matrices
    stimulus_matrices = np.vstack((stimulus_matrices, stimulus_matrix))



class Experiment():
    #stimulus_IDs = []
    #stimulus_matrices = np.array([0,0,0,0,0,0,0])

    def screen_calibration(self, WxH = [], bonsai_range = [3.63, 2.08]): # [3.3, 2.1]
        # give projector image dimensions in mm. Method returns
        # bonsai range is set to highest X or Y value where edge of dot is still visible
        self.WxH = WxH
        self.bonsai_range = bonsai_range
        self.bon_to_cm = (bonsai_range[0] / WxH[0]) * 10
        offset = str(abs(np.round(100-(WxH[0]/WxH[1])/(bonsai_range[0]/bonsai_range[1])*100, 2)))
        print('There is a',offset,'% offset between projector image dimensions and optimal dimensions.',
              'That means stimuli will appear', offset,'% wider than high or vice versa')
        return self.bon_to_cm

    def add_stimulus(self, ID):
        InitializingExperiment.stimulus.append(ID)

    def add_stimulus_matrix(self, stimulus_matrix):
        global stimulus_matrices
        stimulus_matrices = np.vstack((stimulus_matrices, stimulus_matrix))



class Silence():                                            # Stimulus type: 0
    def __init__(self, duration, fps=100):
        self.duration = duration    # Duration in seconds
        self.fps = fps

    def generate_silentperiod(self):
        self.stim_ID = ('silence_'+str(self.duration)+'_'+str(self.fps))
        total_points = self.duration * self.fps     # length of array based on duration * fps
        stim_type_column = np.repeat(0, total_points).tolist()
        filler = np.zeros((total_points,6)).tolist()

        # Produce numpy array with columns: Stimulus type (0). The rest is filler.
        stimulus_matrix = np.column_stack((stim_type_column, filler))
        #return (stimulus_matrix)
        return [stimulus_matrix, self.stim_ID]
        self.stimulus_matrix = stimulus_matrix
        #add_stimulus_matrix(stimulus_matrix)



class Gratings():                                           # Stimulus type: 1
    def __init__(self, angle, duration, temp_freq = 1, spat_freq = 20, fps = 100):
        self.angle = angle
        self.duration = duration
        self.temp_freq = temp_freq
        self.spat_freq = spat_freq
        self.fps = fps
        self.stim_ID = ('grating_'+str(self.duration)+'_'+str(self.fps) +'_'+str(self.angle)+'_'+str(self.temp_freq)+'_'+str(
            self.spat_freq))

    def generate_gratings(self):
        total_points = self.duration * self.fps

        # Generate columns
        stim_type_column = np.repeat(2, total_points).tolist()
        filler = np.zeros((total_points,3)).tolist()
        angle_column = np.repeat(self.angle, total_points).tolist()
        temp_freq_column = np.repeat(self.temp_freq, total_points).tolist()
        spat_freq_column = np.repeat(self.spat_freq, total_points).tolist()

        # Produce numpy array with columns: Stimulus type, filler (in place of Circle or Silent class outputs),
        # angle, and temporal frequency. All values are constant
        stimulus_matrix = np.column_stack((stim_type_column, filler, angle_column, temp_freq_column, spat_freq_column))
        #return(stimulus_matrix)
        return [stimulus_matrix, self.stim_ID]
        self.stimulus_matrix = stimulus_matrix
        #add_stimulus_matrix(stimulus_matrix)



class Circle():                                            # Stimulus type: 2
    def __init__(self, dot_size, radius, duration, clockwise, fps=100):
    # dot_size = diameter (in cm), trajectory radius in cm, duration to complete one circle in seconds,
    # clockwise = boolean,
    # bout_time = time (s) between bouts
    # angle_from & to: if a partial trajectory should be plotted, set the start and end angles here.
    # angle_total_time: the speed of the dot depends on circumference of circle / duration. However, the total stimulus
    #                   duration for a partial angle can be set here. Dot will move forwards and backwards until end
        self.dot_size = dot_size * calib
        self.radius = radius * calib
        self.duration = duration
        self.fps = fps
        if type(clockwise) == bool: self.clockwise = clockwise
        else: print("ERROR: clockwise argument has to be True or False")

        # Generate all points based on total duration of stimulus and fps
        bout_numbers = math.floor(self.duration * self.fps)

        # Generate x and y coordinates based on circumference
        circumference_points = np.linspace(0, 2 * np.pi, bout_numbers)
        r = self.radius
        if self.clockwise == True: x = r * np.sin(circumference_points)
        else: x = r * np.sin(circumference_points) * (-1)
        y = r * np.cos(circumference_points) * (-1)            # Circle always starts at x=0 y=-1 (assuming unit circle)

        # Create matrix based on framerate and duration of presentation
        total_points = self.duration*self.fps           # returns total number of points based on fps + duration of stim
        x_cord = np.repeat(x, total_points/len(y))
        y_cord = np.repeat(y, total_points/len(y))

        # Generate columns:
        stim_type_column = np.repeat(1, total_points).tolist()
        diameter_column = np.repeat(self.dot_size, total_points).tolist()
        # Generate filler coordinates (i.e. coordinate locations based on fps). Produces duplicates based on frame rate
        x_cord = np.append(x_cord, np.repeat(x_cord[len(x_cord)-1], total_points - len(x_cord))).tolist()
        y_cord = np.append(y_cord, np.repeat(y_cord[len(y_cord)-1], total_points - len(y_cord))).tolist()
        # Generate filler columns (in place of other Class outputs)
        filler = np.zeros((total_points,3)).tolist()

        self.stimulus_matrix = np.column_stack((stim_type_column, diameter_column, x_cord, y_cord, filler))

    def continuous(self):
        # Produce numpy array with columns: Stimulus type (always 1), point diameter, X coordinates, Y coordinates, and filler
        stim_ID = ('circlecont_'+str(self.duration)+'_'+str(self.fps)+'_'+str(self.dot_size/calib)+'_'+str(self.radius/calib)+'_'+str(self.clockwise))
        #return (stimulus_matrix)
        return [self.stimulus_matrix, stim_ID]
        stimulus_matrix = self.stimulus_matrix
        #add_stimulus_matrix(stimulus_matrix)

    def bout(self, bout_time):
        stim_ID = ('circlebout_'+str(self.duration)+'_'+str(self.fps)+'_'+str(self.dot_size/calib)+'_'+str(self.radius/calib)+'_'+str(self.clockwise)+'_'+str(bout_time))

        r = self.radius
        bout_numbers = math.floor(self.duration / bout_time)
        circumference_points = np.linspace(0, 2 * np.pi, bout_numbers)
        if self.clockwise == True: x = r * np.sin(circumference_points)
        else: x = r * np.sin(circumference_points) * (-1)
        y = r * np.cos(circumference_points) * (-1)             # Circle always starts at x=0 y=-1 (assuming unit circle)

        total_points = self.duration*self.fps       # returns total number of points based on fps + duration of stim
        x_cord = np.repeat(x, total_points/len(y))
        y_cord = np.repeat(y, total_points/len(y))

        # Generate columns:
        stim_type_column = np.repeat(1, total_points).tolist()
        diameter_column = np.repeat(self.dot_size, total_points).tolist()
        # Generate filler coordinates (i.e. coordinate locations based on fps). Produces duplicates based on frame rate
        x_cord = np.append(x_cord, np.repeat(x_cord[len(x_cord)-1], total_points - len(x_cord))).tolist()
        y_cord = np.append(y_cord, np.repeat(y_cord[len(y_cord)-1], total_points - len(y_cord))).tolist()
        # Generate filler columns (in place of Gratings or Silent Class outputs)
        filler = np.zeros((total_points,3)).tolist()

        stimulus_matrix = np.column_stack((stim_type_column, diameter_column, x_cord, y_cord, filler))
        #return (stimulus_matrix)
        return [stimulus_matrix, stim_ID]
        self.stimulus_matrix = stimulus_matrix
        #add_stimulus_matrix(stimulus_matrix)

    def segment_continuous(self, angle_from, angle_to, segment_duration):   # stimulus travels along circular segment in continuous movement
        stim_ID = ('segcont_' + str(segment_duration) + '_' + str(self.fps) + '_' + str(angle_from) + 'to' + str(angle_to) + '_' + str(
            self.dot_size/calib) + '_' + str(self.radius/calib) + '_' +str(self.duration) + '_' + str(self.clockwise))

        r = self.radius
        x_y_from = [r * np.sin(np.deg2rad(angle_from)), r * np.cos(np.deg2rad(angle_from))]
        x_y_to = [r * np.sin(np.deg2rad(angle_to)), r * np.cos(np.deg2rad(angle_to))]

        stimulus_matrix = self.stimulus_matrix

        row_numbers = []
        for coordinates in [x_y_from, x_y_to]:
            x_y_comparison = []     # compares closest points of each circle quadrant to coordinates determined from angle_from to angle_to
            for i in np.split(stimulus_matrix, 4):
                closest_x = i[:, 2][np.abs(i[:, 2] - coordinates[0]).argmin()]
                closest_y = i[:, 3][np.abs(i[:, 3] - coordinates[1]).argmin()]
                x_y_comparison.append(np.abs(closest_x - coordinates[0]) + np.abs(closest_y - coordinates[1]))
            n_quadrant = np.argmin(x_y_comparison)
            quadrant_length = len(np.split(stimulus_matrix, 4)[0])
            quadrant_rows = [n_quadrant * quadrant_length, n_quadrant * quadrant_length + quadrant_length]
            row_numbers.append(np.abs(stimulus_matrix[quadrant_rows[0]:quadrant_rows[1], 2] - coordinates[0]).argmin() + \
                               quadrant_rows[0])
        if (row_numbers[0] > row_numbers[1]):
            row_numbers = [row_numbers[1], row_numbers[0]]
            stimulus_matrix = stimulus_matrix[row_numbers[0] : row_numbers[1],:][::-1]
        else:
            stimulus_matrix = stimulus_matrix[row_numbers[0]: row_numbers[1], :]
        stimulus_matrix_subset = stimulus_matrix[::-1]

        total_length_in_points = segment_duration * self.fps
        n_repeats = math.floor(total_length_in_points / len(stimulus_matrix_subset))
        for j in range(n_repeats-1):
            if j % 2 == 0:
                stimulus_matrix = np.append(stimulus_matrix, stimulus_matrix_subset, axis=0)
            else:
                stimulus_matrix = np.append(stimulus_matrix, stimulus_matrix_subset[::-1], axis=0)

        # return stimulus_matrix + buffer at end
        if n_repeats % 2 == 0:
            stimulus_matrix = np.append(stimulus_matrix, stimulus_matrix[0:(total_length_in_points - len(stimulus_matrix))], axis=0)
        else:
            stimulus_matrix = np.append(stimulus_matrix,stimulus_matrix[::-1][0:(total_length_in_points - len(stimulus_matrix))], axis=0)

        #return stimulus_matrix
        return [stimulus_matrix, stim_ID]
        self.stimulus_matrix = stimulus_matrix
        #add_stimulus_matrix(stimulus_matrix)

    def segment_bout(self, bout_time, angle_from, angle_to, segment_duration): # stimulus travels along circular segment in bouts
        stim_ID = ('segbout_'+str(segment_duration)+'_'+str(self.fps)+'_'+str(angle_from)+'to'+str(angle_to)+'_'+str(
            self.dot_size/calib)+'_'+str(self.radius/calib)+'_'+str(self.duration)+'_'+str(self.clockwise)+'_'+str(bout_time))

        r = self.radius
        x_y_from = [r * np.sin(np.deg2rad(angle_from)), r * np.cos(np.deg2rad(angle_from))]
        x_y_to = [r * np.sin(np.deg2rad(angle_to)), r * np.cos(np.deg2rad(angle_to))]

        stimulus_matrix = Circle(0.2,2,25,True).stimulus_matrix
        stimulus_matrix = self.stimulus_matrix

        row_numbers = []
        for coordinates in [x_y_from, x_y_to]:
            x_y_comparison = []     # compares closest points of each circle quadrant to coordinates determined from angle_from to angle_to
            for i in np.split(stimulus_matrix, 4):
                closest_x = i[:, 2][np.abs(i[:, 2] - coordinates[0]).argmin()]
                closest_y = i[:, 3][np.abs(i[:, 3] - coordinates[1]).argmin()]
                x_y_comparison.append(np.abs(closest_x - coordinates[0]) + np.abs(closest_y - coordinates[1]))
            n_quadrant = np.argmin(x_y_comparison)
            quadrant_length = len(np.split(stimulus_matrix, 4)[0])
            quadrant_rows = [n_quadrant * quadrant_length, n_quadrant * quadrant_length + quadrant_length]
            row_numbers.append(np.abs(stimulus_matrix[quadrant_rows[0]:quadrant_rows[1], 2] - coordinates[0]).argmin() + \
                               quadrant_rows[0])
        if (row_numbers[0] > row_numbers[1]):
            row_numbers = [row_numbers[1], row_numbers[0]]
            stimulus_matrix = stimulus_matrix[row_numbers[0] : row_numbers[1],:][::-1]
        else:
            stimulus_matrix = stimulus_matrix[row_numbers[0]: row_numbers[1], :]
        stimulus_matrix_subset = stimulus_matrix[::-1]

        total_length_in_points = segment_duration * self.fps
        n_repeats = math.floor(total_length_in_points / len(stimulus_matrix_subset))
        for i in range(n_repeats-1):
            if i % 2 == 0:
                stimulus_matrix = np.append(stimulus_matrix, stimulus_matrix_subset, axis=0)
            else:
                stimulus_matrix = np.append(stimulus_matrix, stimulus_matrix_subset[::-1], axis=0)

        # return stimulus_matrix + buffer at end
        if n_repeats % 2 == 0:
            stimulus_matrix = np.append(stimulus_matrix, stimulus_matrix[0:(total_length_in_points - len(stimulus_matrix))], axis=0)
        else:
            stimulus_matrix = np.append(stimulus_matrix,stimulus_matrix[::-1][0:(total_length_in_points - len(stimulus_matrix))], axis=0)

        bout_fps = int(bout_time*self.fps)
        n_duplicates = math.floor(stimulus_matrix.shape[0]/bout_fps)

        for i in range(n_duplicates):
            stimulus_matrix[:,2:4][i*bout_fps : i*bout_fps+bout_fps] = stimulus_matrix[i*bout_fps,2:4]
        stimulus_matrix[(i*bout_fps+bout_fps):len(stimulus_matrix), 2:4] = stimulus_matrix[(i*bout_fps+bout_fps)-1, 2:4]
        #stimulus_matrix[(i * bout_fps + bout_fps):len(stimulus_matrix), 2:4] = stimulus_matrix[0, 2:4]

        #return stimulus_matrix
        return [stimulus_matrix, stim_ID]
        self.stimulus_matrix = stimulus_matrix

    def segment_bout_jump(self, bout_time, angle_from, angle_to, segment_duration):   # stimulus jumps between two points only
        stim_ID = ('segboutjump_' + str(segment_duration) + '_' + str(self.fps) + '_' + str(angle_from) + 'to' + str(angle_to) + '_' + str(
            self.dot_size/calib) + '_' + str(self.radius/calib) + '_' +str(self.duration) + '_' + str(self.clockwise)+'_'+str(bout_time))

        stim_duration = int(bout_time * self.fps)
        total_points = segment_duration * self.fps  # returns total number of points based on fps + duration of Circle()

        r = self.radius
        x1_y1 = [r * np.sin(np.deg2rad(angle_from)), r * np.cos(np.deg2rad(angle_from))]
        x2_y2 = [r * np.sin(np.deg2rad(angle_to)), r * np.cos(np.deg2rad(angle_to))]

        # Generate stimulus_matrix based on bout_time and limited to duration
        stim_type_column = 1
        diameter_column = self.dot_size

        stimulus_matrix = np.zeros((int(total_points), 7))
        for i in range(math.floor(total_points / stim_duration)):
            if i % 2 == 0:
                stimulus_matrix[(i * stim_duration):(i * stim_duration) + stim_duration] = \
                    [stim_type_column, diameter_column, x1_y1[0], x1_y1[1], 0, 0, 0]
            else:
                stimulus_matrix[(i * stim_duration):(i * stim_duration) + stim_duration] = \
                    [stim_type_column, diameter_column, x2_y2[0], x2_y2[1], 0, 0, 0]

        #return stimulus_matrix
        return [stimulus_matrix, stim_ID]
        self.stimulus_matrix = stimulus_matrix
        #add_stimulus_matrix(stimulus_matrix)








# Set Screen calibration value
calib = Experiment().screen_calibration([142,94])      # save calibration value as object to use in other classes

# Inititate empty list into which to save stimuli
stimuli = []

# set duration of silent period between each stimulus
silence = Silence(5).generate_silentperiod()

# Create all other stimuli and append to stimuli list
stimuli.append(Gratings(angle=0, duration=10, temp_freq = 1, spat_freq = 20, fps = 100).generate_gratings())
stimuli.append(Gratings(angle=180, duration=10, temp_freq = 1, spat_freq = 20, fps = 100).generate_gratings())
stimuli.append(Gratings(angle=90, duration=10, temp_freq = 1, spat_freq = 20, fps = 100).generate_gratings())
stimuli.append(Gratings(angle=270, duration=10, temp_freq = 1, spat_freq = 20, fps = 100).generate_gratings())

stimuli.append(Gratings(angle=0, duration=10, temp_freq = 1, spat_freq = 10, fps = 100).generate_gratings())
stimuli.append(Gratings(angle=180, duration=10, temp_freq = 1, spat_freq = 10, fps = 100).generate_gratings())
stimuli.append(Gratings(angle=90, duration=10, temp_freq = 1, spat_freq = 10, fps = 100).generate_gratings())
stimuli.append(Gratings(angle=270, duration=10, temp_freq = 1, spat_freq = 10, fps = 100).generate_gratings())

stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_bout_jump(bout_time=0.6,angle_from=55.68,angle_to=64.32,segment_duration=10))
stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_bout_jump(bout_time=0.6,angle_from=-55.68,angle_to=-64.32,segment_duration=10))
stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=55.68,angle_to=64.32,segment_duration=10))
stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=-55.68,angle_to=-64.32,segment_duration=10))

stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_bout(bout_time=0.6,angle_from=29.76,angle_to=90.24,segment_duration=10))
stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_bout(bout_time=0.6,angle_from=-29.76,angle_to=-90.24,segment_duration=10))
stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=29.76,angle_to=90.24,segment_duration=10))
stimuli.append(Circle(dot_size=0.2, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=-29.76,angle_to=-90.24,segment_duration=10))

stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_bout_jump(bout_time=0.6,angle_from=55.68,angle_to=64.32,segment_duration=10))
stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_bout_jump(bout_time=0.6,angle_from=-55.68,angle_to=-64.32,segment_duration=10))
stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=55.68,angle_to=64.32,segment_duration=10))
stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=-55.68,angle_to=-64.32,segment_duration=10))

stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_bout(bout_time=0.6,angle_from=29.76,angle_to=90.24,segment_duration=10))
stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_bout(bout_time=0.6,angle_from=-29.76,angle_to=-90.24,segment_duration=10))
stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=29.76,angle_to=90.24,segment_duration=10))
stimuli.append(Circle(dot_size=0.4, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=-29.76,angle_to=-90.24,segment_duration=10))

stimuli.append(Circle(dot_size=2.69, radius=2, duration=25, clockwise=True).segment_bout_jump(bout_time=0.6,angle_from=55.68,angle_to=64.32,segment_duration=5))
stimuli.append(Circle(dot_size=2.69, radius=2, duration=25, clockwise=True).segment_bout_jump(bout_time=0.6,angle_from=-55.68,angle_to=-64.32,segment_duration=5))
stimuli.append(Circle(dot_size=2.69, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=55.68,angle_to=64.32,segment_duration=5))
stimuli.append(Circle(dot_size=2.69, radius=2, duration=25, clockwise=True).segment_continuous(angle_from=-55.68,angle_to=-64.32,segment_duration=5))


# Number of times to repeat stimuli
repeat = 10
random_protocol = True      # Should stimulus order be random? (Random per block, not per experiment)

# Generate complete stimulus matrix ------------------------------------------------------------------------------------
all_stimuli = np.empty((0, stimuli[0][0].shape[1]))
all_stimuli_ID = []

if random_protocol == True:
    for i in range(repeat):
        order = np.random.choice(range(len(stimuli)), len(stimuli), replace=False)
        for i in order:
            all_stimuli = np.append(all_stimuli, stimuli[i][0], axis=0)
            all_stimuli = np.append(all_stimuli, silence[0], axis=0)
            all_stimuli_ID.append(stimuli[i][1])
            all_stimuli_ID.append(silence[1])
else:
    print('yes')
    for i in range(len(stimuli)):
        all_stimuli = np.append(all_stimuli, stimuli[i][0], axis=0)
        all_stimuli = np.append(all_stimuli, silence[0], axis=0)
        all_stimuli_ID.append(stimuli[i][1])
        all_stimuli_ID.append(silence[1])
    all_stimuli = np.tile(all_stimuli,(repeat,1))
print(all_stimuli.shape[0]/100/60)


np.savetxt("coordinates.csv", all_stimuli, delimiter=",", fmt = ("%i","%f","%f","%f","%f","%f","%f"))